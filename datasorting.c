#include<stdio.h>
#include<string.h>
#include<stdlib.h>

void sortmethod(FILE *cf, char *data[], double period[], int linescount)
{
	int i, j;
	double temp;
	char line[110], *s, *end;

	for(i=0; i<linescount; i++)
	{
		fgets(data[i], sizeof(line), cf);							// read file
		strcpy(line, data[i]);										// strtok changes the value of the first argument, hence i firstly copied it
		s = strtok(line, " ");										// split string into tokens
		j=1;
		while(s != NULL)
		{
			if(j==4) period[i] = strtod(s, &end); 					// write the value from 4th column in period array
			s = strtok(NULL, " ");									// set pointer to NULL value after successfully reading token
			j++;
		}
	}

	for(i=1; i<linescount; i++)										// go through the array elements
	{
		for(j=i; j>0; j--)											// once on the ith element, move towards the beginning
		{
			if(period[j]<period[j-1])								// check for inequality between elements next to each other
			{
				temp = period[j];									// if true, swap the neighbouring elements
				period[j] = period[j-1];
				period[j-1] = temp;
				strcpy(line, data[j]);								//do the same for lines
				strcpy(data[j], data[j-1]);
				strcpy(data[j-1], line);
			}
		}
	}
	FILE *sv = fopen("sortmethod.dat", "w");
	for(i=0; i<linescount; i++) fprintf(sv, "%s", data[i]);
	fclose(sv);

	return;
}

void sortmethod2(FILE *cf, char *data2[], double period2[], int linescount)
{
	int i, j;
	double temp;
	char line[110], *s, *end;

	for(i=0; i<linescount; i++)
	{
		fgets(data2[i], sizeof(line), cf);
		strcpy(line, data2[i]);
		s = strtok(line, " ");										// same as before
		j=1;
		while(s != NULL)
		{
			if(j==4) period2[i] = strtod(s, &end);
			s = strtok(NULL, " ");
			j++;
		}
	}

	for(i=0; i<linescount; i++) 									// iterate through every array element
	{
		for(j=i+1; j<linescount; j++)								// iterate through every array element instead of already sorted ones
		{
			if(period2[i] > period2[j])								// check for inequality
			{
				temp = period2[i];									// swap elements when inequality is true
				period2[i] = period2[j];
				period2[j] = temp;
				strcpy(line, data2[i]);								// also replace elements in the lines array
				strcpy(data2[i], data2[j]);
				strcpy(data2[j], line);
			}
		}
	}

	FILE *sv2 = fopen("sortmethod2.dat", "w");
	for(i=0; i<linescount; i++) fprintf(sv2, "%s", data2[i]);		// create and save sorted data in a new file
	fclose(sv2);

	return;
}

int main()
{
	int linescount=0, i;
	double *period, *period2;
	char line[110], **data, **data2;

	FILE *cf = fopen("cepF.dat", "r");

	while(fgets(line, sizeof(line), cf) != NULL) linescount++; 					// count lines in a file
	rewind(cf); 																// refresh the file before next fgets

	period = malloc(linescount*sizeof(double)); 								//
	data = malloc(linescount*sizeof(*data));    								//
	for(i=0; i<linescount; i++) data[i] = malloc(sizeof(line)*sizeof(char));	// dynamically allocate memory for arrays based on number of lines in text file
																				//
	period2 = malloc(linescount*sizeof(double));								//
	data2 = malloc(linescount*sizeof(*data2));									//
	for(i=0; i<linescount; i++) data2[i] = malloc(sizeof(line)*sizeof(char));	//

	sortmethod(cf, data, period, linescount);									// call function to sort the file using first method

	rewind(cf);

	sortmethod2(cf, data2, period2, linescount);								// call function to sort the file using second method

	fclose(cf);

	for(i=0; i<linescount; i++)													//
	{																			//
		free(data[i]);															//
		free(data2[i]);															//
	}																			// free the previously allocated memory
	free(data);																	//
	free(period);																//
	free(data2);																//
	free(period2);																//

	return 0;
}
